'''
Scrapes the web page of a given URL or file, cleans the data, and stores it.
The data is then analyzed and visualized.
'''
import requests
from bs4 import BeautifulSoup
import gzip, os.path
import numpy as np
import json
import pandas as pd
import math
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf
import statsmodels.api as sm
from pandas import Series


def get_soup(url=None,fname=None,gzipped=False):
    '''
    This function takes a url or a file and return a BeautifulSoup object
    Parameters:
    url = string representing a website url,, default value None
    fname = string representing a file name, default value None
    gzipped = Boolean indicating if the url is unzipped, default None
    
    Returns a BeautifulSoup object
    '''
    if fname != None:
        file = open(fname)
        return BeautifulSoup(file, "lxml")
    elif url != None:
        r = requests.get(url)
        if gzipped == True:
            content = gzip.decompress(r.content)
            return BeautifulSoup(content, "lxml")
        return BeautifulSoup(r.content, "lxml")
    else:
        raise RuntimeError('Either url or filename must be specified.')
def save_soup(fname,soup_obj):
    '''
    This function saves a textual representation of a soup object int a file
    Parameters:
    fname: string representing the name of the file to save the soup object to
    soup: a BeautifulSoup object
    
    Returns None
    '''
    file = open(fname, 'w')
    file.write(str(soup_obj))
    file.close()
def scrape_and_save():
    '''
    This function scrapes the given addresses, turns the contents into a soup object
    and saves the soup objects into a file.
    No parameters
    Returns None
    '''
    pcpn = get_soup('http://www.wrcc.dri.edu/WRCCWrappers.py?sodxtrmts+028815+por+por+pcpn+none+msum+5+01+F')
    save_soup('wrcc_pcpn.html',pcpn)
    mint = get_soup('http://www.wrcc.dri.edu/WRCCWrappers.py?sodxtrmts+028815+por+por+mint+none+mave+5+01+F')
    save_soup('wrcc_mint.html',mint)
    maxt = get_soup('http://www.wrcc.dri.edu/WRCCWrappers.py?sodxtrmts+028815+por+por+maxt+none+mave+5+01+F')
    save_soup('wrcc_maxt.html',maxt)
def is_num(str):
    '''
    Function determines whether a string represents an int
    
    Parameters:
    str: string
    
    Returns Boolean, True is string represents an int, Flase if not
    '''
    try:
        float(str)
        return True
    except:
       return False
def load_lists(soup_obj,flag):
    '''
    This function creates a list of lists from the useful infomration of a soup object
    
    Parameters:
    soup_obj: BeautifulSoup object
    flag: string to place in list if there is no data available
    
    Returns a list of lists
    '''
    result = []
    lol = []
    for tr in soup_obj.find_all('tr'):
        row = []
        for td in tr.find_all('td'):
            if td.get_text() == '-----':
                row.append(flag)
            elif is_num(td.get_text()):
                row.append(td.get_text())
        if row != [] and row[0].isdigit():
            lol.append(row)
    for item in lol:
        for i in range(len(item)):
            if i == 0:
                item[0] = int(item[0])
            elif item[i] != flag:
                item[i] = float(item[i])
    for i in range(len(lol[0])):
        result.append([])
    for row in lol:
        for i in range(len(row)):
            result[i].append(row[i])
    return result
def replace_na(lol,r,c,flag,precision=5):
    '''
    This function replaces an element with the average of the surrounding 10 years
    
    Parameters:
    lol: list of lists
    r: row index
    c: column index
    flag: string representing no data
    precision: int for rounding
    
    Returns the average (float)
    '''
    if len(lol[r][:c]) >5:
        before = lol[r][c-5:c]
    else:
        before = lol[r][:c]
    after = lol[r][c+1:c+6]
    sum = 0
    n = 0
    for num in before:
        if num != flag:
            sum += num
            n += 1
    for num in after:
        if num != flag:
            sum += num
            n += 1
    return round((sum/n), precision)
def clean_data(lol,flag,precision=5):
    '''
    This function traverses the list of lists and every time it finds the flag, it calls replace_na to replace the flag
    
    Parameters:
    lol: list of lists
    flag: string representing no data
    precision: int for rounding
    
    Returns None
    '''
    for i in range(len(lol)):
        for j in range(len(lol[i])):
            if lol[i][j]==flag:
                lol[i][j] = replace_na(lol,i,j,flag,precision)
def recalculate_annual_data(lol,avg=False,precision=5):
    '''
    This function recalculates the annual average/total and changes the list in place
    
    Parameters:
    lol: list of lists
    avg: Boolean to calculate the average or a total for a years
    precision: int for rounding precision
    
    Returns None
    '''
    for i in range(len(lol[0])):
        if avg == False:
            total = 0
            for j in range(1,len(lol)-1):
                total += lol[j][i]
            lol[-1][i] = round(total,precision)
        else:
            avg = 0
            for j in range(1,len(lol)-1):
                avg += lol[j][i]
            avg = round(avg,precision)
            lol[-1][i] = round((avg/(len(lol)-2)),precision)
def clean_and_jsonify(lof,flag,precision=5):
    '''
    This function takes a list of filenames and for each file in the first argument, get soup, transform it into a list of lists, clean the list, recalculate the annual data, and store it in a file as a json object.
    
    Parameters:
    lof: list of file names
    flag: string representing no data
    precision: int for rounding precision
    
    Return None
    '''
    for fname in lof:
        fsoup = get_soup(fname=fname)
        flists = load_lists(fsoup,flag)
        clean_data(flists,flag,precision)
        if fname == 'wrcc_pcpn.html':
            recalculate_annual_data(flists, False, precision)
        else:
            recalculate_annual_data(flists, True, precision)
        fname = fname.replace('html', 'json')
        with open(fname, 'w') as fp:
            json.dump(flists, fp)
        fp.close()

def get_panda(fname):
    with open(fname) as fp:
        jlol = json.load(fp)
    index = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec','Ann']
    columns = jlol[0]
    data = jlol[1:]
    return pd.DataFrame(data,index,columns)

def get_stats(df):
    index = ['mean','sigma','s','r']
    columns = df.index
    means = df.mean(axis=1)
    sigmas = df.std(ddof=1)
    s = df.std(ddof=0)
    r = []
    for index in df.index: 
        for col in df.columns:
            month = Series(df.loc[index].values)
            time = Series(df.columns)
            r.append(month.corr(time))
    data = [means,sigmas,s,r]
    return pd.DataFrame(data,index,columns)
   
def smooth_data(df, precision=5):
    data = []
    for i in range(len(df.index)):
        row = []
        for j in range(len(df.columns)):
            if len(df.iloc[i,:j])>=5 and len(df.iloc[i,j:])>=5:
                row.append(df.iloc[i,j-5:j+5].mean())
            elif len(df.iloc[i,:j])>=5:
                row.append(df.iloc[i,j-5:].mean())
            elif len(df.iloc[i,j:])>=5:
                row.append(df.iloc[i,:j+6].mean())
            else:
                row.append(df.iloc[i,:].mean())
        data.append(row)
    return pd.DataFrame(data,df.index,df.columns)
#==========================================================
def main():
    ''' 
    the function checks the current directory for scrape_and_save files.
    If the files are not already in the directory, the websites are scraped
    and saved to the new directory
    '''
    fnames = ['wrcc_pcpn.json','wrcc_mint.json','wrcc_maxt.json']
    for fname in fnames:        
        json_fname = fname.split('.')[0] + '.json'     
        print_stats(json_fname)      
        make_plot(json_fname, precision=2)   
    plt.figure()    
    make_plot(fnames[0].split('.')[0] + '.json', 'Jan')     
    input('Enter to end:')
if __name__ == '__main__':
    main()
