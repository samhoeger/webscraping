'''
This program keeps a directory of Person class objects, and scrapes websites to update the directory if they are 
not already in the directory.
'''

from bs4 import BeautifulSoup



class Person:
    
    def __init__(self,last,first,ptype,email,phone,unit,position,addr,bldg,rm):
        self.last = last
        self.first = first
        self.ptype = ptype
        self.email = email
        self.phone = phone
        self.unit = unit
        self.position = position
        self.addr = addr
        self.bldg = bldg
        self.rm = rm
       
    @classmethod
    def from_soup(cls,soup_obj):
        name = soup_obj.find('h3').get_text().split(', ')
        last = name[0].strip()
        first = name[1].strip()
        email = soup_obj.find('a','mailto').get_text().strip()
        if soup_obj.find('a','phoneto') is not None:
            phone = soup_obj.find('a','phoneto').get_text().strip()
        else:
            phone = ''
        if soup_obj.find('div','department') is not None:
            pos = soup_obj.find('div','department').get_text().strip()
            position = pos.split('\n')[0]
            pos = pos.replace('\r', '')
        else:
            position = ''
        if soup_obj.find('div','degree') is not None:
            unit = soup_obj.find('div','degree').get_text().strip()
        else:
            unit = ''
        if soup_obj.find('span','type') is not None:
            ptype = soup_obj.find('span','type').get_text().strip()
        else:
            ptype = ''
        div_lst = soup_obj.find_all('div')
        if div_lst[-3] is not None:
            addr = div_lst[-3].get_text().strip()
        else:
            addr = ''
        if div_lst[-2] is not None:
            building = div_lst[-2].get_text().strip().split(': ')
            if len(building)>1:
                bldg = building[1]
            else:
                bldg = ''
        else:
            bldg = ''
        if div_lst[-1] is not None:
            room = div_lst[-1].get_text().strip().split(': ')
            if len(room)>1:
                rm = room[1]
            else:
                rm = ''
        else:
            rm = ''
        return cls(last,first,ptype,email,phone,unit,position,addr,bldg,rm)
       
    def generator(self):
        yield self.last+', '+self.first
        yield self.ptype
        yield self.email
        yield self.phone
        yield self.unit
        yield self.position
        yield self.addr
        yield self.bldg+' rm '+self.rm
    def  __repr__(self):
        return self.first+' '+self.last+', '+self.email+', '+self.position
    
    def __eq__(self,other):
        if self.email == other.email:
            return True
        else:
            return False
    
    def __lt__(self,other):
        if self.email == other.email:
            return False
        if self.last > other.last:
            return False
        if self.first > other.first:
            return False
        if self.phone > other.phone:
            return False
        return True
        
    def __hash__(self):
        return hash(self.email)
#==========================================================
def main():
    ''' 
    the function checks the current directory for scrape_and_save files.
    If the files are not already in the directory, the websites are scraped
    and saved to the new directory
    '''
if __name__ == '__main__':
    main()
